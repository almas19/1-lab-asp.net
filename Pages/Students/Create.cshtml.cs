using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace tt.Pages
{
    public class CreateModel : PageModel
    {
        [BindProperty]
        public Student student {get; set;}

        public IActionResult OnPost(){
            if(ModelState.IsValid){
                IndexModel_1.students.Add(student);
                IndexModel_1.static_students.Add(student);
                return RedirectToPage("Index");
            }
            return Page();
        }
    }
}