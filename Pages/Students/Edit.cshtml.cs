using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace tt.Pages
{
    public class EditModel : PageModel
    {
        [BindProperty]
        public static Student student {get; set;}
        public int id;
        public void OnGet(int id)
        {
            student = IndexModel_1.students.Find(s=>s.id==id);
        }
        public IActionResult OnPost(string name, int id){
            if(Regex.IsMatch(name, @"^[a-zA-Z]+$")){
                student.name = name;
                student.id = id;
                return RedirectToPage("Index");
            }
            return Page();
        }
    }
}