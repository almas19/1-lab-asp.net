using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace tt.Pages
{
    public class Student{
        [Display(Name = "StudentID")]
        [Range(1,5000, ErrorMessage="Out of range (1,5000)")]
        public int id {get; set;}
        [Display(Name = "Student name")]
        [Required(ErrorMessage = "You need to give us your name")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage="Use only letters")]
        public string name {get; set;}
    }
    public class IndexModel_1 : PageModel
    {
        public static List<Student> students = new List<Student>(){new Student{name="Almas", id=12312},
                                                            new Student{name="Madina", id=4132},
                                                            new Student{name="Kirill", id=71232},
                                                            new Student{name="Mikhail", id=16542} };
        public static List<Student> static_students = students;                        
        public void OnGet(string sort, int id)
        {
            students.Remove(IndexModel_1.students.Find(s=>s.id==id));
            static_students.Remove(IndexModel_1.students.Find(s=>s.id==id));
            if(sort=="name"){
                students = students.OrderBy(u=>u.name).ToList();
            }
            else{
                students = students.OrderBy(u=>u.id).ToList();
            }
            if(id==-1){
                students = static_students;
            }
        }

       public IActionResult OnPost(string stName){
           if(stName!=null){
                List<Student> newStudents = new List<Student>();
                foreach (var item in students)
                    if(item.name==stName)
                    {
                        newStudents.Add(item);
                    }
                students = newStudents;
                return RedirectToPage("Index");
           }
           else{
               students = static_students;
               return RedirectToPage("Index");
           }
        }

        
    }
}